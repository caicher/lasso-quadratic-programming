function [coeff_best,lambda_best,cv_error,coeffs] = cvRegression(LFunc,ydata,xdata,lambda,frac_train)
% Christopher Aicher
% Regression Project
% 12/3 - Cross-Validation Function 
%
% [coeff_best,lambda_best,cv_error,coeffs] = cvRegression(LFunc,ydata,xdata,lambda,frac_train)
%
% Fits Lfunc on the ydata and xdata for the various lambda values using CV
% Returns the best fit (in terms of CV error) and labmda_best
%Input:
% - LFunc - function of ydata,xdata,lambda
% - ydata,xdata - the regression data; an nx1 vector, nxp matrix
% - lambda - vector of lambda values; ellx1 vector
% - frac_train - fraction of data in train set. (1-frac_train) is the test set
%Output:
% - coeff_best - a px1 vector of coefficients
% - lambda_best - the best lambda value
% - cv_error - the estimated MSE cv_error from numTrials random train/test partitions
%            - ellx2 vector, first column is mean estimate
% - coeffs - pxell sample coefficient matrix
numTrials = 25;
cv_temp = zeros(numTrials,1);
coeffs = zeros(size(xdata,2),numel(lambda));
cv_error = zeros(numel(lambda),2);
train_set = cell(numTrials,1);
test_set = cell(numTrials,1);
for trial = 1:numTrials,
        % Split data into train/test sets
        [train,test] = splitData(ydata,xdata,frac_train);
        train_set{trial} = train;
        test_set{trial} = test;
end
for ell = 1:numel(lambda),
    fprintf('CV Lambda %u of %u\n',ell,numel(lambda));
    for trial = 1:numTrials,
        fprintf('-- Trial %u of %u\n',trial,numTrials);
        % Fit data on train_set
        [~,coeff] = LFunc(train_set{trial}.ydata,train_set{trial}.xdata,lambda(ell));
        % Calculate MSE CV Error
        cv_temp(trial) = mean((test_set{trial}.ydata-test_set{trial}.xdata*coeff).^2);
    end
    cv_error(ell,1) = mean(cv_temp);
    cv_error(ell,2) = std(cv_temp);
    coeffs(:,ell) = coeff;
end
% Find Best Lambda
[~,index] = min(cv_error(:,1));
lambda_best = lambda(index);
[~,coeff_best] = LFunc(ydata,xdata,lambda(index));
fprintf('cvRegression.m Done...\n');
end

% Helper Function
function [train_set,test_set] = splitData(ydata,xdata,frac_train)
% splits the data into two structs, train_set and test_set
% - train_set has two fields ydata and xdata
% - test_set has two fields ydata and xdata
train_index = rand(size(ydata));
thresh = sort(train_index);
thresh = thresh(ceil(numel(thresh)*frac_train));
train_index = train_index <= thresh;
train_set.ydata = ydata(train_index);
train_set.xdata = xdata(train_index,:);
test_set.ydata = ydata(~train_index);
test_set.xdata = xdata(~train_index,:);
end

        