function [x_opt,fval,exit_flag] = myQuadprog(Q,c,A,b,lb,ub)
% Christopher Aicher
% Regression Project
% 12/6 - Quadratic Programming Interior Point Solver
% Algorithm from Vanderbei
%
%Syntax:
% [x_opt,fval,exit_flag] = myQuadprog(Q,c,A,b,lb,ub)
%
%Solves the QP:
%   max  c'x + 1/2 x' Q x
%   s.t. Ax <= b
%         x >= lb
%         x <= ub
%
%Inputs:
% c - nx1 linear cost vector
% Q - nxn quadratic cost vector (Want Q <= 0)
% A - mxn linear constraint matrix
% b - mx1 linear constraint lowerbound
% lb - nx1 lower bound constraint on x
% ub - nx1 upper bound constraint on x
%
%Outputs:
% x_opt - nx1 vector of optimal solution
% fval - the value of the optimal solution
% exit_flag - convergence flag: 1 = optimal solution, 0 = max iter exceeded

% Built-in Parameters
max_iter = 1000;
tol = 10^-3;
delta = 0.1;
r = 0.9;

% Convert to Vanderbei Problem
% min  c1'x + 1/2 x'Q1x
%  st  A1x >= b1
n = numel(c);
A1 = -A;
b1 = -b;
if exist('lb','var') && ~isempty(lb),
    A1 = [A1;eye(n)];
    b1 = [b1;lb];
end
if exist('ub','var') && ~isempty(ub),
    A1 = [A1;-eye(n)];
    b1 = [b1;-ub];
end
c1 = -c;
Q1 = -Q;
[m,n] = size(A1);

% Interior Point Loop
x = ones(n,1);
w = ones(m,1);
y = ones(m,1);
z = ones(n,1);
exit_flag = 0;
for iter = 1:max_iter,
    gamma = z'*x+y'*w;
    mu = max(delta*gamma/(n+m),tol/1.1);
    LHS = [-(diag(z./x)+Q1), A1'; A1, diag(w./y)];
    RHS = [c1 - A1'*y - mu./x+Q1*x ; b1-A1*x+mu./y];
    deltaxy = LHS\RHS;
    deltax = deltaxy(1:n);
    deltay = deltaxy(n+1:end);
    deltaz = (mu - x.*z - z.*deltax)./x;
    deltaw = (mu - y.*w - w.*deltay)./y;
    theta = min([r/max([-deltax./x;-deltay./y;-deltaw./w;-deltaz./z]),1]);
    x = x+theta*deltax;
    w = w+theta*deltaw;
    y = y+theta*deltay;
    z = z+theta*deltaz;
    rho = b1-A1*x + w;
    sigma = c1 - A1'*y - z + Q1*x;
    if max([rho;sigma;mu]) < tol,
        exit_flag = 1;
        break;
    end
    if max(y) > 10^32,
        exit_flag = 0;
        fprintf('Dual looks unbounded');
        break;
    end
    if max(x) > 10^32,
        exit_flag = 0;
        fprintf('Primal looks unbounded');
        break
    end
end
x_opt = x;
fval = c'*x+x'*Q*x/2;



