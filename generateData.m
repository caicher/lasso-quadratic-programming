function [ydata,xdata,ytrue] = generateData(n,coeff,errinv,xinv)
% Christopher Aicher
% Regression Project
% 12/3 - Generate Regression Data
%
% [ydata,xdata,ytrue] = generateData(n,coeff,errinv,xinv)
%
%GENERATEDATA generates synthetic regression data
%
%Inputs:
% - n - the number of data points
% - coeff - px1 vector of coefficients
% - errinv* - the inverse cdf of the noise r.v. (function of a unif r.v.)
% - xinv* - the inverse cdf of the x r.v. (function of a unif r.v.)
%      (*) are optional inputs
%Outputs:
% - ydata - nx1 vector of targets (ydata = noise + ytrue)
% - xdata - nxp vector of features 
% - ytrue - nx1 vector of true values (ytrue = xdata*coeff)
if nargin < 4, xinv = @(x) norminv(x,0,1)*10; end;
if nargin < 3, errinv = @(x) norminv(x,0,1); end;
xdata = xinv(rand(n,length(coeff)));
e = errinv(rand(size(xdata,1),1));
ytrue = xdata*coeff;
ydata = ytrue+e;




