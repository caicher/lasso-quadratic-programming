%Christopher Aicher
%12/18
%Regression Project Example Script File%% Data L2 X, L2 E
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Generate Data L2 X, L2 E
n = 100;
coeff_true = zeros(100,1);
coeff_true(1) = 10;
xinv = @(x) 10*norminv(x,0,1);
errinv = @(x) norminv(x,0,1);
[ydata,xdata,~] = generateData(n,coeff_true,xinv,errinv);
%% Run CV Experiments
L2L1 = @(y,x,lambda) fitGenLasso(y,x,lambda,2,1,0); % Lasso
L2L2 = @(y,x,lambda) fitGenLasso(y,x,lambda,2,2,0); % Ridge
L1L1 = @(y,x,lambda) fitGenLasso(y,x,lambda,1,1,0); % ???
L1L2 = @(y,x,lambda) fitGenLasso(y,x,lambda,1,2,0); % ???
lambda = 0:0.5:6;
fprintf('Fit:L2 Penalty:L1 \n');
[coeff_L2L1,lambda_L2L1,cv_L2L1,coeffsL2L1] = cvRegression(L2L1,ydata,xdata,lambda,0.8);
fprintf('Fit:L2 Penalty:L2 \n');
[coeff_L2L2,lambda_L2L2,cv_L2L2,coeffsL2L2] = cvRegression(L2L2,ydata,xdata,lambda,0.8);
fprintf('Fit:L1 Penalty:L1 \n');
[coeff_L1L1,lambda_L1L1,cv_L1L1,coeffsL1L1] = cvRegression(L1L1,ydata,xdata,lambda,0.8);
fprintf('Fit:L1 Penalty:L2 \n');
[coeff_L1L2,lambda_L1L2,cv_L1L2,coeffsL1L2] = cvRegression(L1L2,ydata,xdata,lambda,0.8);
fprintf('CV Experiment Done...\n');
%% Save Data
save('DataL2XL2E.mat'); fprintf('Data Saved...\n');
%% Plot CV Results
dx = 0.03;
hold all;
errorbar(lambda-dx,cv_L2L1(:,1),cv_L2L1(:,2),'*--');
errorbar(lambda,cv_L2L2(:,1),cv_L2L2(:,2),'o--');
errorbar(lambda+dx,cv_L1L1(:,1),cv_L1L1(:,2),'p--');
errorbar(lambda+2*dx,cv_L1L2(:,1),cv_L1L2(:,2),'s--');
title('Data L2X L2E');
ylabel('CV error');
xlabel('lambda');
legend({'Lasso (L2,L1)','Ridge (L2,L2)', '(L1,L1)','(L1,L2)'});

%% Plot Coeff Distances
coeff_errorL2L1 = zeros(size(cv_L2L1,1),1);
coeff_errorL2L2 = zeros(size(cv_L2L2,1),1);
coeff_errorL1L1 = zeros(size(cv_L1L1,1),1);
coeff_errorL1L2 = zeros(size(cv_L1L2,1),1);
for ii = 1:size(cv_L2L1,1),
    coeff_errorL2L1(ii) = norm(coeff_true-coeffsL2L1(:,ii),2);
    coeff_errorL2L2(ii) = norm(coeff_true-coeffsL2L2(:,ii),2);
    coeff_errorL1L1(ii) = norm(coeff_true-coeffsL1L1(:,ii),2);
    coeff_errorL1L2(ii) = norm(coeff_true-coeffsL1L2(:,ii),2);
end
dx = 0.03;
hold all;
plot(lambda,coeff_errorL2L1,'*--')
plot(lambda,coeff_errorL2L2,'o--')
plot(lambda,coeff_errorL1L1,'p--')
plot(lambda,coeff_errorL1L2,'s--')
title('Data L2X L2E');
ylabel('Coefficient error');
xlabel('lambda');
legend({'Lasso (L2,L1)','Ridge (L2,L2)', '(L1,L1)','(L1,L2)'});
    

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Generate Data L2 X, L1 E
n = 100;
coeff_true = zeros(100,1);
coeff_true(1) = 10;
xinv = @(x) 10*norminv(x,0,1);
errinv = @(x) expinv(x,1).*(2*round(rand(size(x)))-1);
[ydata,xdata,ytrue] = generateData(n,coeff_true,xinv,errinv);
%% Run CV Experiments
L2L1 = @(y,x,lambda) fitGenLasso(y,x,lambda,2,1,0); % Lasso
L2L2 = @(y,x,lambda) fitGenLasso(y,x,lambda,2,2,0); % Ridge
L1L1 = @(y,x,lambda) fitGenLasso(y,x,lambda,1,1,0); % ???
L1L2 = @(y,x,lambda) fitGenLasso(y,x,lambda,1,2,0); % ???
lambda = 0:0.5:10;
fprintf('Fit:L2 Penalty:L1 \n');
[coeff_L2L1,lambda_L2L1,cv_L2L1,coeffsL2L1] = cvRegression(L2L1,ydata,xdata,lambda,0.8);
fprintf('Fit:L2 Penalty:L2 \n');
[coeff_L2L2,lambda_L2L2,cv_L2L2,coeffsL2L2] = cvRegression(L2L2,ydata,xdata,lambda,0.8);
fprintf('Fit:L1 Penalty:L1 \n');
[coeff_L1L1,lambda_L1L1,cv_L1L1,coeffsL1L1] = cvRegression(L1L1,ydata,xdata,lambda,0.8);
fprintf('Fit:L1 Penalty:L2 \n');
[coeff_L1L2,lambda_L1L2,cv_L1L2,coeffsL1L2] = cvRegression(L1L2,ydata,xdata,lambda,0.8);
fprintf('CV Experiment Done...\n');
%% Save Data
save('DataL2XL1E.mat'); fprintf('Data Saved...\n');
%% Plot Results
dx = 0.03;
hold all;
errorbar(lambda-dx,cv_L2L1(:,1),cv_L2L1(:,2),'*--');
errorbar(lambda,cv_L2L2(:,1),cv_L2L2(:,2),'o--');
errorbar(lambda+dx,cv_L1L1(:,1),cv_L1L1(:,2),'p--');
errorbar(lambda+2*dx,cv_L1L2(:,1),cv_L1L2(:,2),'s--');
title('Data L2X L1E');
ylabel('CV error');
xlabel('lambda');
legend({'Lasso (L2,L1)','Ridge (L2,L2)', '(L1,L1)','(L1,L2)'})
%% Plot Coeff Distances
coeff_errorL2L1 = zeros(size(cv_L2L1,1),1);
coeff_errorL2L2 = zeros(size(cv_L2L2,1),1);
coeff_errorL1L1 = zeros(size(cv_L1L1,1),1);
coeff_errorL1L2 = zeros(size(cv_L1L2,1),1);
for ii = 1:size(cv_L2L1,1),
    coeff_errorL2L1(ii) = norm(coeff_true-coeffsL2L1(:,ii),2);
    coeff_errorL2L2(ii) = norm(coeff_true-coeffsL2L2(:,ii),2);
    coeff_errorL1L1(ii) = norm(coeff_true-coeffsL1L1(:,ii),2);
    coeff_errorL1L2(ii) = norm(coeff_true-coeffsL1L2(:,ii),2);
end
dx = 0.03;
hold all;
plot(lambda,coeff_errorL2L1,'*--')
plot(lambda,coeff_errorL2L2,'o--')
plot(lambda,coeff_errorL1L1,'p--')
plot(lambda,coeff_errorL1L2,'s--')
title('Data L2X L1E');
ylabel('Coefficient error');
xlabel('lambda');
legend({'Lasso (L2,L1)','Ridge (L2,L2)', '(L1,L1)','(L1,L2)'});