function [yhat, coeff] = fitGenLasso(y,x,lambda,fit,penalty,constraint)
% Christopher Aicher
% Regression Project
% 12/3 - General Regression Function
%
% [yhat, coeff] = fitGenLasso(y,x,lambda,fit,penalty,constraint)
%
%FITGENLASSO is a general regression function
%Inputs:
% y - nx1 vector of targets
% x - nxp matrix of features
% lambda - penalty coefficient
% fit - fit type (2 - L2, 1 - L1)
% penalty - penalty type (2 - L2, 1 - L1)
% constraint - whether penalty is a hard or soft constraint
%   i.e. min (fit)/n + lambda*(penalty)   - soft constraint (= 0)
%    or  min (fit)/n st penalty < lambda  - hard constraint (= 1)
%Outpus:
% yhat - nx1 vector of estimated targets
% coeff - px1 vector of coefficients

% Defaults
if nargin < 6, constraint = 0; end;
if nargin < 5, penalty = 1; end;
if nargin < 4, fit = 2; end;
if nargin < 3, lambda = 0; end;

A = x;
B = y;
[n p] = size(A);
if ~constraint, 
    % Soft Constraint Case
    M1 = [ A, -A, -eye(n); 
          -A,  A, -eye(n)];
    r1 = [B; -B];
    lb = zeros(2*p+n,1);
    f1 = zeros(2*p+n,1);    
    h1 = zeros(2*p+n,1);
    % Deal with fit
    if fit == 1,
        f1(2*p+1:end) = 1/sqrt(n);
    elseif fit == 2,
        h1(2*p+1:end) = 1/n;
    else
        error('Unrecognized Value of fit = %u',fit);
    end
    % Deal with penalty
    if penalty == 1,
        f1(1:2*p) = lambda;
    elseif penalty == 2,
        h1(1:2*p) = lambda;
    else
        error('Unrecognized Value of penalty = %u',penalty);
    end
    H1 = 2*diag(h1);
else
    % Hard Constraint Case
    error('Hard Constraint Case not implemented yet');
end
% Run Quad Programming Solver    
% options = optimset('Algorithm','interior-point-convex','Display','off');
% if sum(H1(:)) == 0,
%     [coeff,~,exitflag] = linprog(f1,M1,r1,[],[],lb,[],[],options);
% else
%     [coeff,~,exitflag] = quadprog(H1,f1,M1,r1,[],[],lb,[],[],options);
% end

[coeff,~,exitflag] = myQuadprog(-H1,-f1,M1,r1,lb,[]);

if exitflag == 0,
    fprintf('Uh-oh... Max Iterations Exceeded in QuadProg...\n');
end
% Return Answer
coeff = coeff(1:p)-coeff(p+(1:p));
yhat = x*coeff;


